+++
categories = ["Arts", "Programming"]
date = "2015-05-14T03:29:55-05:00"
description = "Making it 'round the world"
slug = ""
tags = []
title = "CraZe On The Web"

+++

I've never had some sort of online **publicity strategy**. Haven't paid for publicity, nor stormed the kind of websites where people go to show their shiny new apps and have others review it. My strategy has been very local, as in sharing stuff to my circle of contacts in **Facebook**, installing it myself to **friends'** devices *(who then install it to their friends and families)*, or sneakily installing or opening it at random **tech stores** in the showcased phones and computers. Locally its a blast doing that, but I've never been sure of how to go around trying to promote an app in the **online world**.

One attempt I did make was nearly 1~2 years ago, I created an [Instagram](https://instagram.com/crazeapp) for drawings made with the [Web](https://craze.herokuapp.com) / [Android](https://play.google.com/store/apps/details?id=com.zubieta.craze) apps, posting images with hashtags hoping for people to find them *(though one thing I didn't do was going into other related pages begging for their followers' attention in comment sections, that is very annoying)*. After a while though, I kinda neglected the account, and I started posting most drawings on **Imgur** privately *([Web Album](https://imgur.com/a/VxwIm), [Android Album](https://imgur.com/a/fdEgK))*. But just the few pictures containing hashtags I had posted on **Instagram** made its way across several users, resulting not only in people knowing about it and liking the pictures, but also some people creating **awesome** stuff with it, even on a constant basis, sometimes with the hashtags, **the prime example being:**

[![](https://i.imgur.com/BwKcxoT.png)](https://instagram.com/space_mandala)

This a very peculiar user. From what I gather, the name is **Eduardo Benko**, a [Psyche-Trance DJ](https://soundcloud.com/djebenko) from *Sao Paulo, Brazil*. His drawings are very crazy and attentive to detail, also mixing a lot of parameters, brushes and palettes in cool ways! Here are some drawings of his, made with the app, but has way more on his [Facebook Page](https://www.facebook.com/djebenko) and [Instagram](https://instagram.com/space_mandala)! I really hope he continues to spread the **love**, and also recommending the app to people :-)

| [![][01]][01] | [![][02]][02] | [![][03]][03] |
|----|----|----|
| [![][04]][04] | [![][05]][05] | [![][06]][06] |

I feel a warm feeling inside akin to **love** when I hear about all kinds of people using the application. People waiting in queues. People using it during show intermissions. People using it at parties. People using it for homework / projects. People using to pass time. But that feeling intensifies when its people looking for **spiritual inspiration**, and also **children** and **elderly people**, which are the most free in mind and eager to express themselves, which is related to the next user I found, [Athaena](https://instagram.com/ooathaenaoo/).

She has a particular sensitivity for nature and artistic stuff, but also has an awesome little cousin who drew these psychedelic _**butterflies ~ papillons**_! I had heard of children using it, but never seen the results of such **beautiful** interaction. A kid does not expect to make something perfect, not even something for others to approve, just to make something as it envisions it to be! Like **Picasso** once said, *'Every child is an artist, the problem is how to remain one when growing up'*. Perhaps when growing up, we start seeking to make things in a way that approaches *'perfection'* *(perhaps a false perfection defined by society standards)*, doing *'rational'* stuff, and while at that also looking for others to approve and like what we do, losing the essence of just doing things how we feel like doing them.

| <blockquote class="instagram-media" data-instgrm-captioned data-instgrm-version="4" style=" background:#FFF; border:0; border-radius:3px; box-shadow:0 0 1px 0 rgba(0,0,0,0.5),0 1px 10px 0 rgba(0,0,0,0.15); margin: 1px; max-width:658px; padding:0; width:99.375%; width:-webkit-calc(100% - 2px); width:calc(100% - 2px);"><div style="padding:8px;"> <div style=" background:#F8F8F8; line-height:0; margin-top:40px; padding:50% 0; text-align:center; width:100%;"> <div style=" background:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACwAAAAsCAMAAAApWqozAAAAGFBMVEUiIiI9PT0eHh4gIB4hIBkcHBwcHBwcHBydr+JQAAAACHRSTlMABA4YHyQsM5jtaMwAAADfSURBVDjL7ZVBEgMhCAQBAf//42xcNbpAqakcM0ftUmFAAIBE81IqBJdS3lS6zs3bIpB9WED3YYXFPmHRfT8sgyrCP1x8uEUxLMzNWElFOYCV6mHWWwMzdPEKHlhLw7NWJqkHc4uIZphavDzA2JPzUDsBZziNae2S6owH8xPmX8G7zzgKEOPUoYHvGz1TBCxMkd3kwNVbU0gKHkx+iZILf77IofhrY1nYFnB/lQPb79drWOyJVa/DAvg9B/rLB4cC+Nqgdz/TvBbBnr6GBReqn/nRmDgaQEej7WhonozjF+Y2I/fZou/qAAAAAElFTkSuQmCC); display:block; height:44px; margin:0 auto -44px; position:relative; top:-22px; width:44px;"></div></div> <p style=" margin:8px 0 0 0; padding:0 4px;"> <a href="https://instagram.com/p/v_WajqLsV9/" style=" color:#000; font-family:Arial,sans-serif; font-size:14px; font-style:normal; font-weight:normal; line-height:17px; text-decoration:none; word-wrap:break-word;" target="_top">So I was fooling around with this #crazeapp when my 5 year old little cousin Shana told me that she wanted to draw a butterfly. This is what she came up with #amazinglightchild #mandala #creativity</a></p> <p style=" color:#c9c8cd; font-family:Arial,sans-serif; font-size:14px; line-height:17px; margin-bottom:0; margin-top:8px; overflow:hidden; padding:8px 0 7px; text-align:center; text-overflow:ellipsis; white-space:nowrap;">A video posted by Athaena (@ooathaenaoo) on <time style=" font-family:Arial,sans-serif; font-size:14px; line-height:17px;" datetime="2014-11-29T16:10:34+00:00">Nov 29, 2014 at 8:10am PST</time></p></div></blockquote> <script async defer src="//platform.instagram.com/en_US/embeds.js"></script> | <blockquote class="instagram-media" data-instgrm-captioned data-instgrm-version="4" style=" height:100%;background:#FFF; border:0; border-radius:3px; box-shadow:0 0 1px 0 rgba(0,0,0,0.5),0 1px 10px 0 rgba(0,0,0,0.15); margin: 1px; max-width:658px; padding:0; width:99.375%; width:-webkit-calc(100% - 2px); width:calc(100% - 2px);"><div style="padding:8px;"> <div style=" background:#F8F8F8; line-height:0; margin-top:40px; padding:50% 0; text-align:center; width:100%;"> <div style=" background:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACwAAAAsCAMAAAApWqozAAAAGFBMVEUiIiI9PT0eHh4gIB4hIBkcHBwcHBwcHBydr+JQAAAACHRSTlMABA4YHyQsM5jtaMwAAADfSURBVDjL7ZVBEgMhCAQBAf//42xcNbpAqakcM0ftUmFAAIBE81IqBJdS3lS6zs3bIpB9WED3YYXFPmHRfT8sgyrCP1x8uEUxLMzNWElFOYCV6mHWWwMzdPEKHlhLw7NWJqkHc4uIZphavDzA2JPzUDsBZziNae2S6owH8xPmX8G7zzgKEOPUoYHvGz1TBCxMkd3kwNVbU0gKHkx+iZILf77IofhrY1nYFnB/lQPb79drWOyJVa/DAvg9B/rLB4cC+Nqgdz/TvBbBnr6GBReqn/nRmDgaQEej7WhonozjF+Y2I/fZou/qAAAAAElFTkSuQmCC); display:block; height:44px; margin:0 auto -44px; position:relative; top:-22px; width:44px;"></div></div> <p style=" margin:8px 0 0 0; padding:0 4px;"> <a href="https://instagram.com/p/v_X6yALsSZ/" style=" color:#000; font-family:Arial,sans-serif; font-size:14px; font-style:normal; font-weight:normal; line-height:17px; text-decoration:none; word-wrap:break-word;" target="_top">5 year old Shana&#39;s #crazeapp #artwork... She called the first one #papillon :)</a></p> <p style=" color:#c9c8cd; font-family:Arial,sans-serif; font-size:14px; line-height:17px; margin-bottom:0; margin-top:8px; overflow:hidden; padding:8px 0 7px; text-align:center; text-overflow:ellipsis; white-space:nowrap;">A video posted by Athaena (@ooathaenaoo) on <time style=" font-family:Arial,sans-serif; font-size:14px; line-height:17px;" datetime="2014-11-29T16:23:42+00:00">Nov 29, 2014 at 8:23am PST</time></p></div></blockquote> <script async defer src="//platform.instagram.com/en_US/embeds.js"></script> |
|----|----|

Right now, the [Web](https://craze.herokuapp.com) / [Android](https://play.google.com/store/apps/details?id=com.zubieta.craze) apps are **free** *(even ad-free)*, the code is hosted at my [GitHub](https://github.com/Zubieta), the **Android** app has over **50.000** lifetime installs *(since June 10, 2013)* with a current install base of almost **10.000** users, and with nearly **1.000** reviews it maintains a great score!

[![](https://i.imgur.com/pc0IUQr.png)](https://i.imgur.com/pc0IUQr.png)

The current distribution of users is as follows. Note that even with **50.000** lifetime installs, only near **10.000** have been retained. I'm looking for ways to improve this.

[![](https://i.imgur.com/NrSqD5I.png)](https://i.imgur.com/NrSqD5I.png)

**Now, these are some of my favorite comments :-)**

[![](https://i.imgur.com/H0QYcvE.png)](https://i.imgur.com/H0QYcvE.png)
<br />
[![](https://i.imgur.com/dolHHLb.png)](https://i.imgur.com/dolHHLb.png)
<br />
[![](https://i.imgur.com/IsYZegg.png)](https://i.imgur.com/IsYZegg.png)
<br />
[![](https://i.imgur.com/VdA7jTK.png)](https://i.imgur.com/VdA7jTK.png)
<br />
[![](https://i.imgur.com/wwVipm2.png)](https://i.imgur.com/wwVipm2.png)
<br />
[![](https://i.imgur.com/ZHBdEVJ.png)](https://i.imgur.com/ZHBdEVJ.png)
<br />
[![](https://i.imgur.com/zbbYZDO.png)](https://i.imgur.com/zbbYZDO.png)
<br />
[![](https://i.imgur.com/YNGwnW7.png)](https://i.imgur.com/YNGwnW7.png)

**But then, not everything could be perfect.** The most common reason of **dislike** *(made evident in the ratings comments)* is the configuration UI, which admittedly, is a hack. I believe this is the biggest deterrent that prevents people from retaining the app. Its a very simple and rudimentary UI, perhaps too much, as its text only *(it being an artistic app which would likely need a rich interface)*.

[![](https://i.imgur.com/NtvlAg8.png)](https://i.imgur.com/NtvlAg8.png)

Practically all of the development efforts have gone on to creating the *'graphics'* stuff, like coding the involved *maths, color things, brushes, effects*, all those *parameters* that gives flexibility to the app. You set them to have crazy combinations of values, and then you draw! The thing is that the UI for altering such parameters is **awful**.

Also, accessing the menu requires a **options button**, which was the norm at **2.2**, when the app was originally made. Now some manufacturers have got rid of the **options button**, replacing it with a **multitasking button**. The way around this is holding the **multitasking button** for a while, but many people do not know that, and its the second main reason of complaints in the reviews, *'I can't access the options menu'*.

**Well, thats it for now, I'll keep this post updated whenever I find in the internet any other peculiar person using CraZe around the world :-)**

[01]: https://i.imgur.com/O0fBa7Y.jpg "Imgur"
[02]: https://i.imgur.com/S0pxGkr.jpg "Imgur"
[03]: https://i.imgur.com/8H4xFmk.jpg "Imgur"
[04]: https://i.imgur.com/VUkW2jf.png "Imgur"
[05]: https://i.imgur.com/vYWcl6M.png "Imgur"
[06]: https://i.imgur.com/e2NqwZr.png "Imgur"