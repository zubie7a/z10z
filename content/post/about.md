+++
categories = ["About"]
date = "0042-06-10T14:21:00-05:00"
description = "Hello Universe!"
slug = "about-me"
tags = ["About"]
title = "About Me"
menu = "main"
+++

I'm a *Computer Science and Engineering* student from [**EAFIT University**](http://www.eafit.edu.co).

**I am a person that loves learning and applying things**, I believe that academia and industry growth go hand in hand, like a feedback loop. I want to seek opportunities that allow me to grow personally and intellectually, something that requires me to think and act on it. I like arts, and aesthetics, not only in the visual part, but also in things that work orderly. I love order, but I think one should have the flexibility to be ready to improvise, compromise, or react on unforeseen situations, and be aware of possible changes in the priorities of tasks and goals. I believe all of this allows me to grow in the sciences and technologies, and growing them, while learning and making nice things.

Most of my programming work experience is regarding **Distributed Systems**, **Information Security**, **Software Engineering**, and **Data Structures and Algorithms**. In my free time I explore things related to **Computational Art**, I made for fun a drawing app involving concepts of trigonometry, geometry, and linear algebra, for making designs with effects, rotations, symmetry, and brushes with over 200.000 downloads, available at the [Android Play Store](https://play.google.com/store/apps/details?id=com.zubieta.craze), also available for [Mobile/Desktop Browser](http://craze-alpha.herokuapp.com), and sometimes I [post psychedelic art](http://instagram.com/crazeapp) made with it.

**Whats in a name?**

The namesake of this blog, **Z10Z**, is a [numeronym](https://en.wikipedia.org/wiki/Numeronym), created with my last names :-)