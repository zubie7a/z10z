+++
categories = ["Movies"]
date = "0042-06-14T00:02:28-05:00"
description = "'The FUCKING Movies'"
slug = ""
draft = true
tags = ["Backlog", "Movies"]
title = "Movies"
+++

...Keeping track of movies I've seen *(that I remember)*, and movies I want to see *(constantly growing!)*. I must've seen way more, but thats hard to remember, so I'll to remember better. I've been tracking seriously only since 2012~2013.

For this purpose, I've created [this tumblr blog](https://thefuckingmovies.tumblr.com), which makes much easier to keep a visual registry in a way that I like, which is displaying just what I felt was the most significant quote and/or still frame of the movie.

| Title | Image |
| ------ | --- |
| 007: Goldfinger | N |
| 007: Casino Royale | Y |
| 007: Quantum of Solace | Y |
| 007: Skyfall | Y |
| 007: SPECTRE | N |
| 10 Things I Hate About You | N |
| 12 Monkeys | Y |
| 12 Years A Slave | N |
| 1984 | Y |
| 2001: A Space Odyssey | Y |
| 300 | N |
| 300: Rise Of An Empire | N |
| '71 | N |
| A Beautiful Mind | N |
| A Clockwork Orange | Y |
| About Time | N |
| Abre Los Ojos | Y |
| Across The Universe | Y |
| Almost Famous | Y |
| Amadeus | N |
| Amelie | N |
| American Beauty | Y |
| American History X | Y |
| American Hustle | Y |
| American Psycho | Y |
| An Education | Y |
| Ant Man | N |
| Apocalypse Now | Y |
| Avatar | N |
| Back To The Future | Y |
| Back To The Future II | Y |
| Back To The Future III | N |
| Barry Lyndon | Y |
| Batman Begins | N |
| Batman The Dark Knight | N |
| Batman The Dark Knight Rises | N |
| Beetlejuice | Y |
| Being John Malkovich | N |
| Big Fish | Y |
| Big Hero 6 | Y |
| Birdman | Y |
| Black Swan | Y |
| Blade Runner | Y |
| Blood Diamond | N |
| Brazil | Y |
| Camp X-Ray | N |
| Captain America: The First Avenger | N |
| Captain America 2: The Winter Soldier | N | 
| Casino | Y |
| Chaplin | Y |
| Children of Men | Y |
| Chinatown | Y |
| Cinema Paradiso | Y |
| Citizenfour | N |
| Cloud Atlas | Y |
| Close Encounters of The Third Kind | N |
| Coffee And Cigarettes | N |
| Coherence | N |
| Contact | N |
| Dallas Buyers Club | Y |
| Dazed And Confused | Y |
| Delicatessen | Y |
| Devil's Advocate | Y |
| Die Welle | N |
| District 9 | N |
| Django Unchained | N |
| Don Jon | Y |
| Donnie Darko | Y |
| Dracula (1992) | Y |
| Dredd (2012) | N |
| Drive | N |
| Dune | N |
| Edge of Tomorrow | Y |
| Edward Scissorhands | Y |
| Enemy | Y |
| Enter The Void | Y |
| Equilibrium | Y |
| Eternal Sunshine of The Spotless Mind | Y |
| Ex Machina | Y |
| Eyes Wide Shut | Y |
| Fargo | N |
| Fear And Loathing In Las Vegas | N |
| Ferris Bueller's Day Off | N |
| Fight Club | Y |
| Forrest Gump | Y |
| Frozen | Y |
| Fullmetal Jacket | N |
| Gattaca | Y |
| Godzilla | Y |
| Gone Girl | N |
| Good Will Hunting | Y |
| Goodfellas | Y |
| Gravity | N |
| Groundhog Day | Y |
| Guardians of The Galaxy | N |
| Hackers | Y |
| Heat | Y |
| High Fidelity | N |
| Hotel Rwanda | Y |
| Hugo | Y |
| I Love You Phillip Morris | Y |
| Ida | N |
| In Bruges | N |
| Inception | Y |
| Indiana Jones: Raiders of The Lost Ark | N |
| Inglorious Basterds | N |
| Inland Empire | N |
| Inside Llewyn Davies | N |
| Interstellar | N |
| Interview With The Vampire | Y |
| Into The Wild | N |
| Jerry McGuire | Y |
| Jodorowsky's Dune | Y |
| Judge Dredd (1995) | N |
| Jumper | N |
| Jupiter Ascending | N |
| Kick-Ass | Y |
| Kick-Ass 2 | N |
| Kill Bill Vol. 1 | N |
| Kill Bill Vol. 2 | N |
| Kingsman | N |
| La Vie En Rose | Y |
| Leon The Professional | Y |
| Les Miserables | N |
| Life of Pi | N |
| Looper | Y |
| Lord of War | N |
| Lost In Translation | Y |
| Machete Kills | N |
| Mad Max | N |
| Mad Max 2: The Road Warrior | N |
| Mad Max 3: Beyond Thunderdome | N |
| Mad Max: Fury Road | N |
| Magnolia | N |
| Marvel's Avengers | N |
| Marvel's Avengers: Age of Ultron | N |
| Mauvais Sang | Y |
| Melancholia | Y |
| Memento | Y |
| Midnight In Paris | Y |
| Miller's Crossing | N |
| Minority Report | N |
| Mississipi Burning | N |
| Moneyball | N |
| Monty Python: The Holy Grail | N |
| Monty Python: Life of Brian | N |
| Monty Python: Meaning of Life | N |
| Moon |
| Moonrise Kingdom | Y |
| Mr. Nobody | Y |
| Much Ado About Nothing | N |
| Mulholland Drive | N |
| Network | N |
| Night On Earth | N |
| Nightcrawler | Y |
| No Country For Old Men | N |
| O Brother Where Art Thou | Y |
| Oceans Eleven | N |
| Oceans Twelve | N |
| Oceans Thirteen | N |
| Office Space | Y |
| Once Upon A Time In America | N |
| Once Upon A Time In The West | N |
| One Flew Over The Cuckoo's Nest | Y |
| Othello | N |
| Pacific Rim | N |
| Palo Alto | Y |
| Pi | N |
| Pink Floyd The Wall | Y |
| Pirate Radio | N |
| Pirates of Silicon Valley | Y |
| Predestination | Y |
| Primer | Y |
| Psycho | N |
| Pulp Fiction | N |
| Raging Bull | N |
| Rainman | Y |
| Requiem For A Dream | N |
| Reservoir Dogs | Y |
| Richard III | Y |
| Ronin | Y |
| Ruby Sparks | N |
| Rush | Y |
| Rushmore | Y |
| Safety Not Guaranteed | N |
| Saving Private Ryan | N |
| Scarface | N |
| Schindler's List | N |
| Scott Pilgrim V.S. The World | Y |
| Se7en | Y |
| Serpico | Y |
| Shame | Y |
| Shutter Island | N |
| Silver Linings Playbook | Y |
| Slumdog Millionaire | N |
| Snowpiercer | N |
| Source Code | Y |
| Speedracer | N |
| Star Trek | N |
| Star Trek Into Darkness | Y |
| Star Wars I: The Phantom Menace | N |
| Star Wars II: Attack of The Clones | N |
| Star Wars III: Revenge of The Sith | N |
| Star Wars IV: A New Hope | N |
| Star Wars V: The Empire Strikes Back | N |
| Star Wars VI: Return of The Jedi | N |
| Star Wars VII: The Force Awakens | N |  
| Starship Troopers | N |
| State Of Play | N |
| Stoker | N |
| Straight Outta Compton | N |
| Strangers On A Train | Y |
| Submarine | Y |
| Super 8 | N |
| Suspiria | Y |
| Swordfish | N |
| Synecdoche, New York | Y |
| Taxi Driver | Y |
| Team America World Police | N |
| The Aviator | N |
| The Big Lebowski | Y |
| The Bourne Identity | N |
| The Bourne Supremacy | N |
| The Bourne Ultimatum | N |
| The Bourne Legacy | N |
| The Breakfast Club | Y |
| The City of Lost Children | Y |
| The Curious Case of Benjamin Button | N |
| The Darjeeling Limited | Y |
| The Departed | Y |
| The Devil Wears Prada | N |
| The Dreamers | Y |
| The Fountain | Y |
| The French Connection | N |
| The Girl With The Dragon Tattoo | N |
| The Godfather | Y |
| The Godfather II | Y |
| The Godfather III | Y |
| The Grand Budapest Hotel | N |
| The Great Gatsby | N |
| The Green Mile | N |
| The Hobbit: An Unexpected Journey | N |
| The Hobbit: The Desolation of Smaug | N |
| The Hobbit: The Battle of The Five Armies | N |
| The Hunger Games | Y |
| The Hunger Games Catching Fire | Y |
| The Hunger Games Mockingjay Pt. I | N |
| The Hunger Games Mockingjay Pt. II | N |
| The Imitation Game | N |
| The Internship | Y |
| The Intouchables | N |
| The Island | Y |
| The Judge | Y |
| The Last of The Mohicans | N |
| The Life Aquatic With Steve Zissou | Y |
| The Limits of Control | Y |
| The Lord of The Rings: Fellowship of The Ring | N |
| The Lord of The Rings: The Two Towers | N |
| The Lord of The Rings: Return of The King | N |
| The Machinist | Y |
| The Man From Earth | Y |
| The Martian | N |
| The Matrix | N |
| The Matrix Reloaded | N |
| The Matrix Revolutions | N |
| The Merchant of Venice | N |
| The Perks of Being A Wallflower | Y |
| The Pianist | Y |
| The Prestige | Y |
| The Road | N |
| The Royal Tenenbaums | Y |
| The Science of Sleep | Y |
| The Shawshank Redemption | Y |
| The Shining | Y |
| The Social Network | Y |
| The Thin Red Line | Y |
| The Time Traveler's Wife | Y |
| The Tree of Life | N |
| The Truman Show | Y |
| The Usual Suspects | Y |
| The Untouchables | N |
| The Wolf of Wall Street | Y |
| The Zero Theorem | Y |
| Trainspotting | Y |
| Transcendence | Y |
| TRON Legacy | Y |
| True Grit | N |
| Up | N |
| Upside Down | Y |
| V For Vendetta | Y |
| Vanilla Sky | Y |
| Vanishing Point | N |
| Vertigo | N |
| Videodrome | N |
| Wall-E | N |
| Wargames | N |
| Watchmen | Y |
| What Dreams May Come | Y |
| Whiplash | Y |
| Wild Tales (Relatos Salvajes) | N |
| Willy Wonka & The Chocolate Factory (1971) | Y |
| X-Men First Class | N |
| X-Men Days of Future Past | Y |
| Zombieland | Y |