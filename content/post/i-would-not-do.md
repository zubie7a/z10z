+++
categories = ["Life"]
date = "2013-12-07T02:19:23-05:00"
description = "Some midnight realizations."
slug = "i-would-not"
tags = ["Realizations"]
title = "I Would (Not)"
+++

There's things I’ve said **I’d not want**, or **never do**, or **never be**, and saw such things as negative, and nonetheless I've ended up **wanting**, **doing**, or **becoming** many of them.

_‘I’d **never** study at `____` , such an shallow place’_  
_'I’d **never** stop talking with `____` , it means a lot to me’_  
_'I’d **never** put `____` before `____` , its a matter of principles’_  
_'I’d **never** want to learn about `____` , or stuff related to it’_  
_'I’d **never** become like `____` , it gets on my nerves'_  
_'I'd **never** dedicate myself to `____`, such an meaningless way of life'_  
_'I'd **never** like anyone that isn't `____`'_

*And many others mental statements like that...*  

Then there's the things I’ve wanted to pursue, or thought to myself that I must want, do, or become, which are the things I ended up myself achieving the less, or not at all. For some reason it was apparently more probable for me to get to do something I say I must not do, or rather not do, than getting to something I say I must do or I want to do.

Then I came to the realization that some of the things I’ve said **'not’** to do before, were part of how I really was, or am, things I had forgotten that were part of what defined myself, but no matter how much I tried fighting against them, I'd end up drawn back to them.

Such thoughts, lifestyles, ways of acting and doing things, **that were the most like myself**, started being eroded away by some **external pressures** *(such as being labeled an outcast, and also the fear of not living up to the expectations many people has in myself)*, some stupid or broken **moral or social imperatives** *(like, you must be this way if you ever want to amount to anything)*, and some stupid desire of **wanting to live the lives of others and not my own** *(as in, trying to live, act and work the way the people I admired lived, acted and worked, because I thought that by doing so I'd achieve the success for which I admired them, that it would be easier following their trail, rather than making my own path)*.

But then I'd always find really hard to adapt myself to such things I and mistakenly saw as **good**, then feeling bad because of such failed attempts, and then being drawn back with an inmense force to the things I mistakenly saw as **bad**, but weren't bad, just were the things most like me, that I forced myself to see as bad just trying to fit in, live up to the expectations of others, and living the lives of others.

**The solution of these issues can come down to, trying to ignore the most obnoxious broken moral or social imperatives** *(self-fulfillment, at the cost of possibly not being what others and society defines as good, or acceptable)*, **trying to be and live with who you are** *(not shaping your life to the lives of others, their ideas of success or what they perceive as good or acceptable)*, **and not trying to be and live up to what others want you to be and live up to** *(not shaping your life according to the pressure coming from their expectations, living up to what you, and not others, want of yourself)*.