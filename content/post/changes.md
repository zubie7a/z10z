+++
categories = ["Life"]
date = "0042-06-11T18:31:27-05:00"
description = "Trying styles."
slug = "changes"
tags = ["Beard", "Changes", "Hair", "Haircut", "Styles"]
title = "Changes"

+++

Some pictures I've taken along many years *(mostly while I have been at university since 2011)*, years full of **changes**, going from thinking and doing **kinda stupid stuff**, to thinking and doing either **kinda more rational stuff** or **stupider stuff** heh, a whole life learning, but also, just out of pure curiosity, trying several combinations of **hair styles**, sometimes even cutting my own hair myself to **try things**. *(Or sometimes experimenting odd stuff just for one day).*

*Long hair, short hair, really short hair, messy hair, ironed hair, no beard, long beard, trimmed beard, [just-one-day strange beard](https://i.imgur.com/08QYPLZ.jpg), unkempt hair, kempt hair*, **in any combination**, you name it.

Ever since I got a digital camera around 2006, I've taken a **LOT** of pictures. You'd think I take a lot of selfies, but pictures of myself account to less than 1% of all pictures I've ever taken *(Right now, my computer holds almost 52000 pictures I've taken from all sort of occassions, birthdays, travels, concerts, events, school, university, or just plain daily life).*

So, the same way I register in this post my **personal change**, I also try to structure and organize in my computer all the pictures I've taken during more than **10 years** now from all sorts of **experiences**, as they hold not only dear memories but are proof of the passage of time and how much I've **learned**, **enjoyed** and **lived** during all these years, **together** with my family, friends, teachers, and many more people whom I've crossed paths with!

| June 2, 2007 | February 28, 2008 | February 28, 2008 |
|---|---|---|
| [![][A1]][A1] | [![][A2]][A2] | [![][A3]][A3] |

| June 10, 2008 | December 9, 2008 | December 13, 2008 |
|---|---|---|
| [![][A4]][A4] | [![][A5]][A5] | [![][A6]][A6] |

| June 20, 2009 | July 1, 2009 | October 8, 2009 |
|---|---|---|
| [![][A7]][A7] | [![][A8]][A8] | [![][A9]][A9] |

| December 22, 2009 | May 25, 2010 | October 28, 2010 |
|---|---|---|
| [![][A10]][A10] | [![][A11]][A11] | [![][A12]][A12] |

| September 12, 2011 | September 25, 2011 | October 31, 2011 |
|---|---|---|
| [![][01]][01] | [![][02]][02] | [![][03]][03] |

| January 13, 2012 | January 23, 2012 | February 9, 2012 |
|---|---|---|
| [![][82]][82] | [![][04]][04] | [![][81]][81] |

| March 21, 2012 | March 28, 2012  | April 9, 2012 |
|---|---|---|
| [![][83]][83] | [![][05]][05] | [![][06]][06] |

| May 22, 2012 | May 27, 2012 | May 31, 2012 |
|---|---|---|
| [![][07]][07] | [![][08]][08] | [![][A13]][A13] |

| September 18, 2012 | November 9, 2012 | November 23, 2012 |
|---|---|---|
| [![][09]][09] | [![][A14]][A14] | [![][10]][10] |

| November 24, 2012 | December 1, 2012 | January 7, 2013 |
|---|---|---|
| [![][11]][11] | [![][A15]][A15] |[![][12]][12] |

| January 23, 2013 | January 24, 2013 | January 25, 2013 |
|---|---|---|
| [![][13]][13] | [![][14]][14] | [![][15]][15] |

| April 16, 2013 | April 16, 2013 | May 28, 2013 |
|---|---|---|
| [![][16]][16] | [![][85]][85] | [![][84]][84] 

| June 11, 2013 | July 5, 2013 | July 15, 2013 |
|---|---|---|
| [![][86]][86] | [![][17]][17] | [![][18]][18] |

| August 1, 2013 | August 5, 2013 | August 13, 2013 |
|---|---|---|
| [![][19]][19] | [![][20]][20] | [![][87]][87] |

| August 21, 2013 | August 23, 2013 | August 27, 2013 |
|---|---|---|
| [![][88]][88] | [![][21]][21] | [![][89]][89] |

| September 11, 2013 | September 15, 2013 | October 15, 2013 |
|---|---|---|
| [![][23]][23] | [![][22]][22] | [![][24]][24] |

| January 18, 2014 | January 20, 2014 | March 11, 2014 |
|---|---|---|
| [![][26]][26] | [![][25]][25] | [![][90]][90] |

| March 29, 2014 | June 3, 2014 | June 3, 2014 |
|---|---|---|
| [![][92]][92] | [![][27]][27] | [![][91]][91] |

| August 9, 2014 | October 7, 2014 | October 31, 2014 |
|---|---|---|
| [![][93]][93] | [![][28]][28] | [![][94]][94] |

| November 12, 2014 | November 24, 2014 | December 17, 2014 |
|---|---|---|
| [![][29]][29] | [![][95]][95] | [![][A16]][A16] |

| December 26, 2014 | January 1, 2015 | January 26, 2015 |
|---|---|---|
| [![][96]][96] | [![][30]][30] | [![][31]][31] |

| January 26, 2015 | January 27, 2015 | January 30, 2015 |
|---|---|---|
| [![][97]][97] | [![][32]][32] | [![][98]][98] |

| February 6, 2015 | February 8, 2015 | February 13, 2015 |
|---|---|---|
| [![][A17]][A17] | [![][A18]][A18] | [![][99]][99] |

| February 16, 2015  | February 16, 2015 | February 22, 2015 |
|---|---|---|
| [![][100]][100]  | [![][33]][33]  | [![][34]][34] |

| April 26, 2015 | May 10, 2015 | May 12, 2015 |
|---|---|---|
| [![][35]][35] | [![][A19]][A19] | [![][36]][36] | 

| June 6, 2015 | June 19, 2015 | July 13, 2015 |
|---|---|---|
| [![][A20]][A20] | [![][A21]][A21] | [![][37]][37] | 

| September 17, 2015 | September 18, 2015 | September 19, 2015 |
|---|---|---|
| [![][38]][38] | [![][39]][39] | [![][40]][40] |

| October 16, 2015 | October 31, 2015 | November 11, 2015 |
|---|---|---|
| [![][A35]][A35] | [![][A36]][A36] | [![][A37]][A37] |

| November 21, 2015 | December 2, 2015 | December 8, 2015 |
|---|---|---|
| [![][42]][42] | [![][41]][41] | [![][A22]][A22] |

| December 15, 2015 | December 20, 2015 | January 20, 2015 |
|---|---|---|
| [![][A23]][A23] | [![][A24]][A24] | [![][43]][43] |

| February 10, 2016 | February 16, 2016 | March 29, 2016 |
|---|---|---|
| [![][44]][44] | [![][45]][45] | [![][46]][46] |

| April 2, 2016 | April 15, 2016 | May 8, 2016 |
|---|---|---|
| [![][47]][47] | [![][48]][48] | [![][49]][49] |

| June 12, 2016 | July 2, 2016 | July 24, 2016 |
|---|---|---|
| [![][50]][50] | [![][51]][51] | [![][52]][52] |

| August 20, 2016 | August 22, 2016 | September 6, 2016 |
|---|---|---|
| [![][53]][53] | [![][54]][54] | [![][55]][55] |

| September 8, 2016 | September 26, 2016 | October 7, 2016 |
|---|---|---|
| [![][56]][56] | [![][57]][57] | [![][58]][58] |

| October 16, 2016 | October 25, 2016 | October 31, 2016 |
|---|---|---|
| [![][A25]][A25] | [![][A26]][A26] | [![][59]][59] |

| November 7, 2016 | November 20, 2016 | November 21, 2016 |
|---|---|---|
| [![][60]][60] | [![][A27]][A27] | [![][61]][61] |

| November 21, 2016 | November 28, 2016 | November 30, 2016 |
|---|---|---|
| [![][62]][62] | [![][63]][63] | [![][A29]][A29] |

| December 10, 2016 | December 11, 2016 | December 15, 2016 |
|---|---|---|
| [![][A30]][A30] | [![][A31]][A31] | [![][A32]][A32] |
 

| December 17, 2016 | December 26, 2016 | January 8, 2017 |
|---|---|---|
| [![][64]][64] | [![][A33]][A33] |  [![][A34]][A34] |

[A1]: https://i.imgur.com/YfwXPBC.jpg "Imgur"
[A2]: https://i.imgur.com/dyCj2eH.jpg "Imgur"
[A3]: https://i.imgur.com/sLaDTnY.jpg "Imgur"
[A4]: https://i.imgur.com/8hzhx0F.jpg "Imgur"
[A5]: https://i.imgur.com/FEBeNGp.jpg "Imgur"
[A6]: https://i.imgur.com/CZjOcCi.jpg "Imgur"
[A7]: https://i.imgur.com/prS5ICA.jpg "Imgur"
[A8]: https://i.imgur.com/bQIYJU4.jpg "Imgur"
[A9]: https://i.imgur.com/8YEescK.jpg "Imgur"
[A10]: https://i.imgur.com/u1fVtoc.jpg "Imgur"
[A11]: https://i.imgur.com/0UqC1OO.jpg "Imgur"
[A12]: https://i.imgur.com/48zU5tV.jpg "Imgur"
[A13]: https://i.imgur.com/00xwVIv.jpg "Imgur"
[A14]: https://i.imgur.com/nEHlSxO.jpg "Imgur"
[A15]: https://i.imgur.com/d76Q9dg.jpg "Imgur"
[A16]: https://i.imgur.com/S0oLSBT.jpg "Imgur"
[A17]: https://i.imgur.com/orYxXXB.jpg "Imgur"
[A18]: https://i.imgur.com/vcFtwPB.jpg "Imgur"
[A19]: https://i.imgur.com/ciK6qRH.jpg "Imgur"
[A20]: https://i.imgur.com/vpNUAxi.jpg "Imgur"
[A21]: https://i.imgur.com/DIjAwPR.jpg "Imgur"
[A22]: https://i.imgur.com/e2KKJEH.jpg "Imgur"
[A23]: https://i.imgur.com/ienEsgs.jpg "Imgur"
[A24]: https://i.imgur.com/VOSfGYi.jpg "Imgur"
[A25]: https://i.imgur.com/QOO9iNr.jpg "Imgur"
[A26]: https://i.imgur.com/Xu7XuR8.jpg "Imgur"
[A27]: https://i.imgur.com/0N1xZZh.jpg "Imgur"
[A28]: https://i.imgur.com/PtWACVK.jpg "Imgur"
[A29]: https://i.imgur.com/Prmpi5m.jpg "Imgur"
[A30]: https://i.imgur.com/bfzQkUB.jpg "Imgur"
[A31]: https://i.imgur.com/d81pT3c.jpg "Imgur"
[A32]: https://i.imgur.com/5zjMWHF.jpg "Imgur"
[A33]: https://i.imgur.com/KYAwHF7.jpg "Imgur"
[A34]: https://i.imgur.com/d0PS1Cg.jpg "Imgur"
[A35]: https://i.imgur.com/ECruPUv.jpg "Imgur"
[A36]: https://i.imgur.com/yA7FCZv.jpg "Imgur"
[A37]: https://i.imgur.com/c4qQwX2.jpg "Imgur"

[01]: https://i.imgur.com/MlP3PJy.jpg "Imgur"
[02]: https://i.imgur.com/fjtZtMV.jpg "Imgur"
[03]: https://i.imgur.com/IEFGyyx.jpg "Imgur"
[04]: https://i.imgur.com/kj6TZfX.jpg "Imgur"
[05]: https://i.imgur.com/bWnidmA.jpg "Imgur"
[06]: https://i.imgur.com/yrzwyDu.jpg "Imgur"
[07]: https://i.imgur.com/jrIFyRP.jpg "Imgur"
[08]: https://i.imgur.com/dmAPgHz.jpg "Imgur"
[09]: https://i.imgur.com/CTKS2Oj.jpg "Imgur"
[10]: https://i.imgur.com/08QYPLZ.jpg "Imgur"
[11]: https://i.imgur.com/PqRr19S.jpg "Imgur"
[12]: https://i.imgur.com/BP9OVc4.jpg "Imgur"
[13]: https://i.imgur.com/qAOGi4Q.jpg "Imgur"
[14]: https://i.imgur.com/vS0jdQr.jpg "Imgur"
[15]: https://i.imgur.com/swRt13o.jpg "Imgur"
[16]: https://i.imgur.com/qfBQVCt.jpg "Imgur"
[17]: https://i.imgur.com/AMQnnDK.jpg "Imgur"
[18]: https://i.imgur.com/9J8gJy2.jpg "Imgur"
[19]: https://i.imgur.com/KNiKfZI.jpg "Imgur"
[20]: https://i.imgur.com/3Y7TRC1.jpg "Imgur"
[21]: https://i.imgur.com/uIpdQT0.jpg "Imgur"
[22]: https://i.imgur.com/wSH2vox.jpg "Imgur"
[23]: https://i.imgur.com/MhNbBtD.jpg "Imgur"
[24]: https://i.imgur.com/ivcneBn.jpg "Imgur"
[25]: https://i.imgur.com/QTEO7kA.jpg "Imgur"
[26]: https://i.imgur.com/BRE4myS.jpg "Imgur"
[27]: https://i.imgur.com/nzL5SdX.jpg "Imgur"
[28]: https://i.imgur.com/BfZ9cVy.jpg "Imgur"
[29]: https://i.imgur.com/ahx8LFr.jpg "Imgur"
[30]: https://i.imgur.com/88uNY75.jpg "Imgur"
[31]: https://i.imgur.com/Gd9lbMU.jpg "Imgur"
[32]: https://i.imgur.com/F8lqC0I.jpg "Imgur"
[33]: https://i.imgur.com/33UlaAT.jpg "Imgur"
[34]: https://i.imgur.com/gTHmNpD.jpg "Imgur"
[35]: https://i.imgur.com/Jl6exa6.jpg "Imgur"
[36]: https://i.imgur.com/9MVBtTO.png "Imgur"
[37]: https://i.imgur.com/Y1gk7cf.jpg "Imgur"
[38]: https://i.imgur.com/FOKWqBE.jpg "Imgur"
[39]: https://i.imgur.com/TxiSP1e.png "Imgur"
[40]: https://i.imgur.com/JqIL8w0.jpg "Imgur"
[41]: https://i.imgur.com/hUO9i0Q.jpg "Imgur"
[42]: https://i.imgur.com/wM1spFH.png "Imgur"
[43]: https://i.imgur.com/fxgVyad.jpg "Imgur"
[44]: https://i.imgur.com/KfzuJR6.jpg "Imgur"
[45]: https://i.imgur.com/88s7U1L.png "Imgur"
[46]: https://i.imgur.com/6ILSFaX.jpg "Imgur"
[47]: https://i.imgur.com/UfaXrnT.jpg "Imgur"
[48]: https://i.imgur.com/9WgzaiS.png "Imgur"
[49]: https://i.imgur.com/BKStozk.jpg "Imgur"
[50]: https://i.imgur.com/hCHlKZ1.jpg "Imgur"
[51]: https://i.imgur.com/b8uBMrC.png "Imgur"
[52]: https://i.imgur.com/3NNQxe8.jpg "Imgur"
[53]: https://i.imgur.com/k5MwdBI.jpg "Imgur"
[54]: https://i.imgur.com/eTfOd67.png "Imgur"
[55]: https://i.imgur.com/PhjjdKn.jpg "Imgur"
[56]: https://i.imgur.com/QS9ntXX.jpg "Imgur"
[57]: https://i.imgur.com/xCu42Ay.png "Imgur"
[58]: https://i.imgur.com/IiEM9ym.jpg "Imgur"
[59]: https://i.imgur.com/12QTcMY.jpg "Imgur"
[60]: https://i.imgur.com/Yx6wXNz.png "Imgur"
[61]: https://i.imgur.com/SswpVEU.jpg "Imgur"
[62]: https://i.imgur.com/PtWACVK.jpg "Imgur"
[63]: https://i.imgur.com/vP08pQ7.png "Imgur"
[64]: https://i.imgur.com/Mle7Vpa.png "Imgur"


[81]: https://i.imgur.com/VZ6utWn.jpg "Imgur"
[82]: https://i.imgur.com/r4dPl0G.jpg "Imgur"
[83]: https://i.imgur.com/RnXPBim.png "Imgur"
[84]: https://i.imgur.com/NsfqoyJ.jpg "Imgur"
[85]: https://i.imgur.com/lc6sdPN.jpg "Imgur"
[86]: https://i.imgur.com/YiD9zWT.png "Imgur"
[87]: https://i.imgur.com/APOSfZj.jpg "Imgur"
[88]: https://i.imgur.com/q1j6TZ1.jpg "Imgur"
[89]: https://i.imgur.com/KBcr7LC.png "Imgur"
[90]: https://i.imgur.com/O9TcmZ2.jpg "Imgur"
[91]: https://i.imgur.com/ShRaqp4.jpg "Imgur"
[92]: https://i.imgur.com/Jr6TkwK.png "Imgur"
[93]: https://i.imgur.com/aPuWQ1L.jpg "Imgur"
[94]: https://i.imgur.com/lBXAuOQ.jpg "Imgur"
[95]: https://i.imgur.com/z7o2IJl.png "Imgur"
[96]: https://i.imgur.com/h0PVflz.jpg "Imgur"
[97]: https://i.imgur.com/EI1dvwK.jpg "Imgur"
[98]: https://i.imgur.com/IZAJUzK.png "Imgur"
[99]: https://i.imgur.com/stZbkn3.png "Imgur"
[100]: https://i.imgur.com/Yje1Jcz.png "Imgur"
